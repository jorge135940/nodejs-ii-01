const express = require('express');

class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT || 8080;
    this.personaPath = '/api/persona';
    this.middlewares();
    this.routes();
  }

  // Método para lectura y parseo del body
  middlewares() {
    this.app.use(express.json());
    this.app.use(express.static('public'));
  }

  // Método para las rutas
  routes() {
    this.app.use(this.personaPath, require('../routes/personaRoutes'));
  }

  // Escucha el puerto
  listen() {
    this.app.listen(this.port, () => {
      console.log('Servidor corriendo en el puerto ', this.port);
    });
  }

}


module.exports = Server;